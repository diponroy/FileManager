﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Web;
using FileManager.Model;

namespace FileManager.Web.Models
{
    public class Uow
    {
        //private const string Storagelocation = "..\\..\\..\\..\\Storage\\Files";  /*not at root folder*/
        private const string Storagelocation = "\\Storage\\Files";  /*at root folder*/

        public StorageManager Storage
        {
            get
            {
                return new StorageManager(MappedPath(Storagelocation));
            }
        }

        public Uploader Uploader
        {
            get
            {
                return new Uploader(MappedPath(Storagelocation));
            }
        }
        
        private string MappedPath(string path)
        {
            var root = Path.GetFullPath(HttpContext.Current.Server.MapPath("~"));
            root = System.IO.Path.GetFullPath(root + path);
            return root;
        }
    }
}