﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using FileManager.Model;
using FileManager.Web.Models;
using Workflow.Model;

namespace FileManager.Web.Controllers
{
    public class DemoController : Controller
    {
        private Uow Uow { get; set; }

        public DemoController()
        {
            Uow = new Uow();
        }
        public ActionResult Index()
        {
            return View();
        }

        #region Folder
        [HttpGet]
        public JsonResult AllFolder()
        {
            var result = Uow.Storage.AllFolderFromRoot();

            //http://stackoverflow.com/questions/14591750/asp-net-mvc4-setting-the-default-json-serializer
            //http://www.dalsoft.co.uk/blog/index.php/2012/01/10/asp-net-mvc-3-improved-jsonvalueproviderfactory-using-json-net/

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(result.data);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //defenetly need to do some thing here.
        public JsonResult AddFolder(string pathFromRoot, string name)
        //public JsonResult AddFolder(TestBinding binding )
        {
            var apiResult = Uow.Storage.AddDerectory(pathFromRoot, name);
            return Json(apiResult, JsonRequestBehavior.AllowGet);  //this is picked up by client as jqXhr.responseObject 
        }

        [HttpPost]
        public JsonResult RenameFolder(string pathFromRoot, string oldName, string newName)
        {
            var apiResult = Uow.Storage.RenameDerectory(pathFromRoot, oldName, newName);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveFolder(string pathFromRoot, string name)
        {
            var apiResult = Uow.Storage.RemoveDerectory(pathFromRoot, name);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }   
        #endregion

        #region Files

        [HttpPost]
        public JsonResult AllFile(string pathFromRoot, string folderName)
        {
            var apiResult = Uow.Storage.AllFile(pathFromRoot, folderName);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //need to work on it, how to use apiResutl here
        /*http://venkatbaggu.com/file-upload-in-asp-net-mvc-using-dropzone-js-and-html5/*/
        public JsonResult AddFile(string pathFromRoot, string folderName)
        {
            //string message = Uow.Uploader.Upload(pathFromRoot, folderName, Request.Files);
            //return Json(new { Message = message }, JsonRequestBehavior.AllowGet);


            bool isSavedSuccessfully = true;
            string fName = "";
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {


                        string pathString = Uow.Uploader.FullPath(pathFromRoot, folderName);

                        var fileName1 = Path.GetFileName(file.FileName);

                        bool isExists = System.IO.Directory.Exists(pathString);

                        if (!isExists)
                            System.IO.Directory.CreateDirectory(pathString);

                        var path = string.Format("{0}\\{1}", pathString, file.FileName);
                        file.SaveAs(path);

                    }

                }

            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            if (isSavedSuccessfully)
            {
                return Json(new { Message = fName });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }
        }

        [HttpPost]
        public JsonResult RenameFile(string pathFromRoot, string oldName, string newName)
        {
            var apiResult = Uow.Storage.RenameFile(pathFromRoot, oldName, newName);
            return Json( apiResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveFile(string pathFromRoot, string name)
        {
            var apiResult = Uow.Storage.RemoveFile(pathFromRoot, name);
            return Json(apiResult, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

    #region test code
    public class TestBinding{
        public string pathFromRoot {get;set;}

        public string name { get; set; }
        public string oldName { get; set; }
        public string newName { get; set; }

    }
    #endregion
}