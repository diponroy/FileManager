﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager.Model
{
    public class Folder
    {
        public Folder Parent {get;set;}

        public long Id {get;set;}
        public string Name {get;set;}
        public string PathFromRoot { get; set; }
        public List<Folder> Children {get;set;}

        public string DisplayName
        {
            get
            {
                return Name;
            }
        }
    }
}
