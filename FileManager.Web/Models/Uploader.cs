﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FileManager.Web.Models
{
    public class Uploader
    {
        public readonly string Root;
        public Uploader(string root)
        {
            Root = root;
        }
        public string FullPath(string path, string name)
        {
            var root = Root;
            root += (string.IsNullOrEmpty(path)) ? "" : "\\" + path;
            root += (string.IsNullOrEmpty(name)) ? "" : "\\" + name;
            return root;
        }
        public string Upload(string pathFromRoot, string folderName, HttpFileCollectionBase files)
        {

            bool isSavedSuccessfully = true;
            string fileName = "";
            try
            {
                foreach (string name in files)
                {
                    HttpPostedFileBase file = files[name];
                    if (file != null && file.ContentLength > 0)
                    {
                        fileName = file.FileName;
                        string pathString = FullPath(pathFromRoot, folderName);
                        var path = String.Format("{0}\\{1}", pathString, file.FileName);
                        file.SaveAs(path);
                    }
                }
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }


            string msg = (isSavedSuccessfully) ? fileName : "Error in saving file";
            return msg;
        }
    }
}