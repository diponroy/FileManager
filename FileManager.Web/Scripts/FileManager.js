﻿/*all used urls*/
var url = {
    folders: {
        all: fileroot + 'AllFolder',        //get
        add: fileroot + 'AddFolder',        //post
        rename: fileroot + 'RenameFolder',  //post
        remove: fileroot + 'RemoveFolder',  //post
    },

    files: {
        allForFolder: fileroot + 'AllFile', //post
        upload: fileroot + 'AddFile',
        rename: fileroot + 'RenameFile',    //post
        remove: fileroot + 'RemoveFile',    //post
    }
};


/*error enum*/
var errorCode = {
    unprocessableEntity: 422
};


/* API */
/*generic api for POST and GET
 * param contains optional spinner dom element and operation.  Note the spinner element can be any element not necessarily the modal dialog
   if (param.spinner is set) then show a spinner on that element
 */

function api(type, url, param, data, donehandler, failhandler) {
    param = param || {};
    var spinner = null;  //paired with param.spinner

    if (param.spinner) {
        spinner = new Spinner().spin(param.spinner);
    }

    //if (spinner) {
    //    alert('spinner is defined');
    //}

    function stopSpinner()
    {
        if (spinner) {
            spinner.stop();
            delete spinner;
        }
    }

    var retval = $.ajax({
        type: type, //"POST",
        contentType: "application/json",
        url: url,
        data: JSON.stringify(data)
    })
        .done(function (json, textStatus, jqXhr) {
            //turn spinner off
            stopSpinner();
            donehandler(json, textStatus, jqXhr);
        })
        .fail(function (jqXhr, textStatus, errorThrown) {
            //turn spinner off
            stopSpinner();

            if (typeof failhandler != 'undefined') {
                failhandler(jqXhr, textStatus, errorThrown);
                return;
            }
            var msg = (param.operation|| '') + ' failure. ' + jqXhr.status + ' ' + jqXhr.statusText;
            toastr.error(msg);
        });
    return retval;
}


/*
param : 
    param.spinner the spiner to show at, $('#divFolderContainer')[0]
    parame.operation the message, to point which (function/action)
*/
function apiPost(url, param, data, donehandler, failhandler) {
    return api('POST', url, param, data, donehandler, failhandler);
}

function apiGet(url, param, data, donehandler, failhandler) {
    return api('GET', url, param, data, donehandler, failhandler);
}


/*generic GET*/

function apiGetOLD(url, data, donehandler, failhandler) {
    var retval = $.ajax({
        contentType: "application/json",
        url: url,
        data: data,
    })
        .done(function(json, textStatus, jqXhr) {
            donehandler(json, textStatus, jqXhr);
        })
        .fail(function(jqXhr, textStatus, errorThrown) {
            if (typeof failhandler != 'undefined') {
                failhandler(jqXhr, textStatus, errorThrown);
                return;
            }
            var msg = 'get failure. ' + jqXhr.status + ' ' + jqXhr.statusText;
            toastr.error(msg);
        });
    return retval;
}

/*reforming the total path*/
var pathMaker = function(path, subPath) {
    var totalPath = "";
    totalPath += (!path || path.length === 0) ? '' : path;
    totalPath += (totalPath.length === 0 && subPath && subPath.length > 0)
        ? subPath
        : (!subPath || subPath.length === 0)
            ? ''
            : '\\' + subPath;
    return totalPath;
};


/*folder view model*/
function FolderListVm() {

    var isRootFolder = function(pathFromRoot, name) {
        return pathFromRoot === "" && name === "";
    };

    var self = this;
    
    var folderModel = function (data) {
        var own = this;
        own.allType = ko.observableArray([
            { text: 'Folder', value: 0, cssClass: '' },
            { text: 'Image', value: 1, cssClass: 'image' }
        ]);

        own.id = ko.observable(data.id);
        own.collapseIndicatorId = ko.computed(function () {
            return 'chk' + own.id();
        }, this);

        own.name = ko.observable(data.name);
        own.displayName = ko.observable(data.displayName);
        own.pathFromRoot = ko.observable(data.pathFromRoot);
        own.parentid = ko.observable(data.parentid);
        own.type = ko.observable(data.type);
        own.typeClass = ko.computed(function () {
            return $.grep(own.allType(), function (item) { return item.value === own.type(); })[0].cssClass;
        }, this);
        own.selected = ko.observable(data.selected);     //when is clicked
        own.expended = ko.observable(data.expended);
    };

    /*folders*/
    self.folders = ko.observableArray([]);
    self.root = ko.computed(function() {
        return _.filter(self.folders(), function(item) { return item.parentid() == null; });
    }, this);
    self.children = function (parentId) {
        var folders = _.filter(self.folders(), function (item) { return item.parentid() === parentId(); });
        return _.sortBy(folders, function (folder) { return folder.name(); });
    };
    
    /*modal props*/
    self.formTitle = ko.observable('');
    self.id = ko.observable();
    self.name = ko.observable('');
    self.oldName = ko.observable('');
    self.parentId = ko.observable();
    self.pathFromRoot = ko.observable();

    /*modals*/
    self.addModal = $('#folder-add-modal');
    self.renameModal = $('#folder-rename-modal');


    self.add = function () {
        var param = { spinner: $('#folder-add-modal form')[0], operation: 'Add folder' };
        var data = {
            pathFromRoot: self.pathFromRoot(),
            name: self.name()
        };
        apiPost(url.folders.add, param, data,
            function(json) {
                switch (json.code) {
                    case 0:
                        self.addModal.modal('hide');
                        
                        /*add created folder*/
                        var maxId = _.max(self.folders(), function (item) { return item.id(); }).id();
                        var newFolder = new folderModel({
                            id: ++maxId,
                            name: self.name(),
                            displayName: self.name(),
                            parentid: self.id(),
                            pathFromRoot: self.pathFromRoot(),
                            type: 0,
                            selected: false,
                            expended: false
                        });
                        var folders = self.folders();
                        folders.push(newFolder);
                        self.folders(folders);

                        /*add hover style the new folder*/
                        var folderDom = $('#' + newFolder.collapseIndicatorId()).siblings('.actionHolder:first');
                        $(folderDom).hover(
                            function () {
                                $(".treeview .actionHolder.hovered").each(function () {
                                    $(this).trigger("mouseout");
                                });
                                $(this).addClass("hovered");
                            },
                            function () {
                                $(this).removeClass("hovered");
                            });

                        /*set selected style*/
                        setTimeout(function () {
                            $('.folderType').on('click', function (event) {
                                event.stopPropagation();
                            });

                            $(folderDom).find('span.title:first').trigger("click");
                        }, 50);
                        

                        /*expand the parent folder*/
                        var parentFolder = _.filter(self.folders(), function (item) { return item.id() === newFolder.parentid(); })[0];
                        parentFolder.expended(true);

                        toastr.success('Folder Added.', 'Success.');
                        break;
                        
                    case errorCode.unprocessableEntity:
                        toastr.warning('Folder name in use.', 'Warning.');
                        break;
                    
                    default:
                        toastr.info('Unknown error code', 'Unknown');
                        break;
                }
            },
            function() {
                toastr.error('Error to add folder', 'Error.');
            });
    };

    self.showToAdd = function(folder) {
        /*PROB: doesn't work, if defined at the vm definition*/
        self.addModal = $('#folder-add-modal');  //TODO: move this to the ViewModel definition code
        self.formTitle('Add Folder');
        self.id(folder.id());
        self.parentId(folder.parentid());
        self.name('');
        self.pathFromRoot(pathMaker(folder.pathFromRoot(), folder.name()));
        self.addModal.modal('show');        
    };

    self.rename = function() {
        var param = { spinner: $('#folder-rename-modal form')[0], operation: 'Rename folder' };
        var data = {
            pathFromRoot: self.pathFromRoot(),
            oldName: self.oldName(),
            newName: self.name()
        };

        apiPost(url.folders.rename, param, data,
            function(json) {
                switch (json.code) {
                    case 0:
                        self.renameModal.modal('hide');

                        /*modify name*/
                        var folder = _.filter(self.folders(), function(item) { return item.id() === self.id(); })[0];
                        folder.name(self.name());
                        folder.displayName(self.name()); /*IMP: this is dislayped at the tree*/                       

                        toastr.success('Folder renamed.', 'Success.');
                        break;

                    case errorCode.unprocessableEntity:
                        toastr.warning('Folder name in use.', 'Warning.');
                        break;

                    default:
                        toastr.info('Unknown error code', 'Unknown');
                        break;
                }
            },
            function() {
                toastr.error('Error to rename folder', 'Error.');
            });
    };

    self.showToRename = function (folder) {
        /*IMPT: don't let to remove the root folder*/
        if (isRootFolder(folder.pathFromRoot(), folder.name())) {
            bootbox.alert("Cann't remove the root folder.");
            return;
        }
        
        self.renameModal = $('#folder-rename-modal');   /*PROB: doesn't work, if defined at the vm definition*/
        self.formTitle('Rename folder');
        self.id(folder.id());
        self.parentId(folder.parentid());
        self.pathFromRoot(folder.pathFromRoot());
        self.oldName(folder.name());
        self.name(folder.name());
        self.renameModal.modal('show');

    };

    self.remove = function (folder) {
        var param = { spinner: $('#divFolderContainer')[0], operation: 'Remove folder' };
        var data = {
            pathFromRoot: folder.pathFromRoot(),
            name: folder.name()
        };
        apiPost(url.folders.remove, param, data,
            function(json) {
                if (json.code === 0) {
                    toastr.success('Folder removed.', 'Success.');
                    
                    var subFolders = self.children(folder.parentid); /*IMP: not parentid(), see how self.children works*/
                    var folderIndex = null;
                    for (var i = 0; i < subFolders.length; i++) {
                        if (subFolders[i].id() == folder.id()) {
                            folderIndex = i;
                            break;
                        }
                    }

                    var nextFolder = (subFolders[i + 1]) ? subFolders[i + 1] : null;
                    var prevFolder = (subFolders[i - 1]) ? subFolders[i - 1] : null;
                    var folderToHilite = null;
                    if (nextFolder != null) {
                        folderToHilite = nextFolder;
                    } else if (nextFolder == null && prevFolder != null) {
                        folderToHilite = prevFolder;
                    } else {
                        folderToHilite = _.filter(self.folders(), function (item) { return item.id() == folder.parentid(); })[0];
                    }

                    /*remove folder and all of its childes*/
                    var stack = [];
                    stack.push(folder);
                    while (stack.length > 0) {
                        var aFolder = stack.pop();
                        var folderToRemove = _.filter(self.folders(), function (item) { return item.id() === aFolder.id(); })[0];
                        var childes = _.filter(self.folders(), function (item) { return item.parentid() === folderToRemove.id(); });
                        $.each(childes, function (index, childFolder) {
                            stack.push(childFolder);
                        })
                        self.folders.remove(folderToRemove);
                    }

                    /*set selected style to next, prev, or parent*/
                    setTimeout(function () {
                        $('#' + folderToHilite.collapseIndicatorId()).siblings('.actionHolder:first').find('span.title:first').trigger("click");
                    }, 50);

                    return;
                }
            },
            function() {
                toastr.error('Error to remove folder', 'Error.');
            });
    };

    self.confirmToRemove = function(folder) {
        /*IMPT: don't let to remove the root folder*/
        if (isRootFolder(folder.pathFromRoot(), folder.name())) {
            bootbox.alert("Cann't remove the root folder.");
            return;
        }
        bootbox.confirm("Do you want to remove this folder ?", function(confirmed) {
            if (confirmed) {
                self.remove(folder);
            }
        });
    };

    self.load = function () {
        var param = { spinner: $('#divFolderContainer')[0], operation: 'Load folder' };
        return apiGet(url.folders.all, param, {},
            function(json) {
                if (json.code === 0) {
                    self.folders([]);
                    
                    json.data.DisplayName = "root";
                    json.data.ParentId = null;   /*impt: to assign null*/
                    
                    var folders = [];
                    var stack = [];                 
                    stack.push(json.data);
                    while (stack.length > 0) {
                        var current = stack.pop();
                        folders.push(
                            new folderModel({
                                id: current.Id,
                                name: current.Name,
                                displayName: current.DisplayName,
                                pathFromRoot: current.PathFromRoot,
                                parentid: current.ParentId,
                                type: 1,
                                selected: false,
                                expended: true
                            })
                        );

                        if (current.Children == null) {
                            continue;
                        }
                        for (var i = 0; i < current.Children.length; i++) {
                            var child = current.Children[i];
                            child.ParentId = current.Id;
                            stack.push(child);
                        }
                    }

                    self.folders(folders);
                    
                    /*hover style to all folders*/
                    $(".treeview .actionHolder").each(function () {
                        $(this).hover(function () {
                            // Mouseover state
                            $(".treeview .actionHolder.hovered").each(function () {
                                $(this).trigger("mouseout");
                            });
                            $(this).addClass("hovered"); // <- for example
                        },
                                function () {
                                    // Mouseout state
                                    $(this).removeClass("hovered");
                                });
                    });

                    /*when try to change folder type, remove click event*/
                    $('.folderType').on('click', function (event) {
                        event.stopPropagation();
                    });
                    return;
                }
            },
            function() {
                toastr.error('Error to load folders', 'Error.');
            }
        );
    };
}

/*file view model*/

function FileListVm() {

    /*dropzone*/
    var myDropzone = new Dropzone("#file-dropzone", { url: url.files.upload });

    var self = this;
    /*bread crumb*/
    self.pathFromRoot = ko.observable(''); //important to initialize for breadcrumb
    self.folderName = ko.observable('');
    self.breadcrumb = ko.computed(function() {
        var root = "\\" + pathMaker(self.pathFromRoot(), self.folderName());
        return root;
    }, this);

    self.files = ko.observableArray([]);

    self.renameModal = $('#file-rename-modal');
    self.name = ko.observable(),
    self.type = ko.observable(),
    self.oldName = ko.observable(),
    self.pathFromRoot = ko.observable(),

    self.rename = function () {
        var param = { spinner: $('#file-rename-modal form')[0], operation: 'Rename folder' };
        var data = {
            pathFromRoot: self.pathFromRoot(),
            oldName: self.oldName(),
            newName: self.name() + self.type()
        };
        apiPost(url.files.rename, param, data,
            function(json) {
                switch (json.code) {
                    case 0:
                        self.renameModal.modal('hide');
                        self.reload();  //TODO: need refactor
                        toastr.success('File renamed.', 'Success.');
                        break;

                    case errorCode.unprocessableEntity:
                        toastr.warning('File name in use.', 'Warning.');
                        break;

                    default:
                        toastr.info('Unknown error code', 'Unknown');
                        break;
                }
            },
            function() {
                toastr.error('Error to rename file', 'Error.');
            });
    };

    self.showToRename = function (item) {
        self.renameModal = $('#file-rename-modal');
        self.oldName(item.Name);
        self.name(item.Name.replace(new RegExp(item.Type + '$'), ''));
        self.type(item.Type);
        self.pathFromRoot(item.PathFromRoot);
        self.renameModal.modal('show');
    };

    self.remove = function (item) {
        var param = { spinner: $('#divFileContainer')[0], operation: 'Load Folder' };
        var data = {
            pathFromRoot: item.PathFromRoot,
            name: item.Name
        };
        apiPost(url.files.remove, param, data,
            function(json) {
                if (json.code === 0) {
                    toastr.success('File removed.', 'Success.');
                    self.reload();      //TODO: need refactor
                    return;
                }
            },
            function() {
                toastr.error('Error to remove file', 'Error.');
            }
        );
    };

    self.confirmToRemove = function(item) {
        bootbox.confirm("Do you want to delete this file ?", function(confirmed) {
            if (confirmed) {
                self.remove(item);
            }
        });
    };

    self.load = function (pathFromRoot, folderName) {
        var param = { spinner: $('#divFileContainer')[0], operation: 'Load Folder' };
        var data = {
            pathFromRoot: pathFromRoot,
            folderName: folderName
        };
        apiPost(url.files.allForFolder, param, data,
            function(json) {
                if (json.code === 0) {
                    data = json.data;
                    self.files([]);
                    for (var i = 0; i < data.length; i++) {
                        data[i].DisplayModifiedDateTime = moment(data[i].ModifieDateTime).format("LLLL");
                    }
                    self.files(data);
                    
                    /*on mouse live, hide the action and options*/
                    $('#tbl_file_holder tr').each(function () {
                        $(this).mouseleave(function () {
                            if ($(this).find(".dropdown").hasClass('open')) {
                                $(this).find(".dropdown").removeClass('open');
                                $(this).find(".dropdown button:first").attr('aria-expanded', 'false').blur();
                            }
                        });
                    });
                    return;
                }
            },
            function() {
                toastr.error('Error to load file', 'Error.');
            }
        );
    };

    self.reload = function() {
        self.load(self.pathFromRoot(), self.folderName());
    };

    self.reSetDropzone = function() {
        myDropzone.removeAllFiles();
        myDropzone.on("sending", function(file, xhr, formData) {
            formData.append("pathFromRoot", self.pathFromRoot());
            formData.append("folderName", self.folderName());
        });
        myDropzone.on("complete", function(file) {
            if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                toastr.success('Uploaded all the files.');
                self.reload();      //TODO: here each file need to be added to the container.
            }
        });
    };

    self.showUploader = function () {
        myDropzone.removeAllFiles();
        $('.dropzone_holder').fadeIn().show();
    };
    
    self.hideUploader = function() {
        $('.dropzone_holder').fadeOut().hide();
    };
}

/*main vm*/

function ViewModel() {

    var self = this;
    /*child vms*/
    self.folderListVm = new FolderListVm();
    self.fileListVm = new FileListVm();

    self.showFiles = function (folder) {
        /*load files of the folder, and initialize dropzone*/
        self.fileListVm.pathFromRoot(folder.pathFromRoot());
        self.fileListVm.folderName(folder.name());
        self.fileListVm.reload();
        self.fileListVm.reSetDropzone();
        
        /*deselect previous selected*/
        var selectedFolders = _.filter(self.folderListVm.folders(), function (item) { return item.selected() === true; });
        $.each(selectedFolders, function (index, aFolder) {
            aFolder.selected(false);
        });
        folder.selected(true);        
    };
}

$(document).ready(function() {
    var vm = new ViewModel();
    ko.applyBindings(vm);

    /*impt: load folders first then root files*/
    var calls = [];
    calls.push(vm.folderListVm.load());
    $.when.apply(this, calls).done(function () {
        /*load files for root folder*/
        $(".treeview ul:first span:first").trigger('click');
        /*make refresh enable*/
        $("#resizable #btnRefresh").removeClass('disable_a_href');
        $("#mirror #btnRefresh").removeClass('disable_a_href');
        $("#mirror #btnUpload").removeClass('disable_a_href');
    });
});


