﻿/*bootstrap tooltip*/
$(function () {
    $('body').tooltip({
        selector: "[data-tooltip=tooltip]",
        container: "body"
    });
});

/*dropzone*/
$(function () {
    Dropzone.autoDiscover = false;
    Dropzone.options.myDropzone = {
        maxFilesize: 70,          // MB  100,
        //maxThumbnailFilesize: 2000  //MB
    };
});


/*section translate*/
var translate = function (sectionToShow, sectionToHide) {
    $(sectionToHide).fadeOut().hide();
    $(sectionToShow).fadeIn().show();
};