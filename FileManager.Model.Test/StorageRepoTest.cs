﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace FileManager.Model.Test
{
    [TestClass]
    public class StorageRepoTest
    {

        //private const string root = @"E:\ODesk\ATILLC\New folder\New folder\Storage\Files";
        private StorageManager Repo { get; set; }
        private const string path = "Important";


        [TestInitialize]
        public void TestInitialize()
        {
            /*
             * the folder and file structur
             * 
             *          -- Files
             *              -- Important
             *                  -- Housing Docs
             *                      -- Sales
             *                      -- Rents
             *                  -- Office Docs
             *                  -- readme.txt
             *                  -- todo.txt
             * 
             */


            var root = AppDomain.CurrentDomain.BaseDirectory;
            root = Path.GetFullPath(root + "..\\..\\..\\..\\..\\..\\Storage\\Files");
            Repo = new StorageManager(root);


            if (Directory.Exists(root + "\\" + path))
            {
                System.IO.DirectoryInfo downloadedMessageInfo = new DirectoryInfo(root + "\\" + path);
                foreach (FileInfo file in downloadedMessageInfo.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in downloadedMessageInfo.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(root + "\\" + path);
            }

            File.Create(root + "\\readme.txt");
            File.Create(root + "\\todo.txt");

            Directory.CreateDirectory(root + "\\" + path);
            Directory.CreateDirectory(root + "\\" + path + "\\Housing Docs");
            Directory.CreateDirectory(root + "\\" + path + "\\Housing Docs\\Sales");
            Directory.CreateDirectory(root + "\\" + path + "\\Housing Docs\\Rents");
            Directory.CreateDirectory(root + "\\" + path + "\\Office Docs");

            File.Create(root + "\\" + path + "\\readme.txt");
            File.Create(root + "\\" + path + "\\todo.txt");
        }

        [TestMethod]
        public void CreateFolder()
        {
            Repo.AddDerectory(path, "New Folder");
        }

        [TestMethod]
        public void RenameFolder()
        {
            Repo.RenameDerectory(path, "Housing Docs", "Housing Docs 1");
        }

        [TestMethod]
        public void DeleteFolder()
        {
            Repo.RemoveDerectory(path, "Office Docs");
        }

        [TestMethod]
        public void AllRecusiveFolder()
        {
            var value = Repo.AllFolderFromRoot();
        }


        /*problem with the initialized files, need t*/
        [TestMethod]
        public void RenameFile()
        {
            Repo.RenameFile(path, "readme.txt", "readme1.txt");
        }

        [TestMethod]
        public void DeleteFile()
        {
            Repo.RemoveFile(path, "todo.txt");
        }

        [TestMethod]
        public void AllFile()
        {
            var files = Repo.AllFile("", path);
        }
    }
}
