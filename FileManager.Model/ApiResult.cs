﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workflow.Model
{
    public class ApiResult
    {
        public int code { get; set; }
        public string message { get; set; }

        public int count;     // count of db operation results. Not serialized.

        public object data { get; set; }

        //public ApiResult(ApiResultCode code, string message = null)
        //{
        //    this.code = (int)code;
        //    this.message = message ?? code.ToString();
        //}

        public ApiResult(int code = 0, string message = null)
        {
            this.code = code;
            this.message = message;
        }

        public ApiResult(object data)
        {
            this.code = 0;
            this.message = "OK";
            this.data = data;
        }

        public void Save(int count, string failure ="FAILURE")
        {
            this.count = count;
            message = (count > 0) ? "OK" : failure;
        }

        public static ApiResult SUCCESS(string statusText = null)
        {
            return new ApiResult(0, statusText);
        }

        public static ApiResult FAILURE(string statusText = null)
        {
            return new ApiResult(1, statusText);
        }


        //public static ApiResult ERROR(int errorCode, string statusText = null)
        //{
        //    return new ApiResult(errorCode, statusText) { data = errorCode };
        //}


        public bool Success
        {
            get { return this.code == 0; }
        }
    }

    public partial class MyDbContext
    {
        public enum CanOperation
        {
            // A project member can always read, if Active.
            Add,    //must be a RW member of the project. Rights are R, RW
            Edit,   //must be a RW member of the project
            Delete, //must be a RW member of the project
            View,   //handled by the controller
            List    //handled by the controller
        }

        //public ApiResult CanCheck(CanOperation operation, string entityType)
        //{
        //    this.CurrentUserId
        //    if (CurrentUser.IsAdmin)
        //        return ApiResult.Ok();

        //    var can = GetCanVariable(operation.ToString() + entityType);
        //    if (can.HasValue)
        //        return new ApiResult(can.Value ? 0 : 1, "debug");

        //    return new ApiResult(2, "No rights");
        //}

    }

}
