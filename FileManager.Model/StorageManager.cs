﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Workflow.Model;

namespace FileManager.Model
{
    //need to work with the excetions assigns, and test codes
    public class StorageManager
    {
        private const string _defaultFile = "test.html";
        public string Root { get; private set; } 
        public StorageManager(string root)
        {
            if (String.IsNullOrEmpty(root) || !Directory.Exists(root))
            {
                throw new Exception("root is not found");
            }
            Root = root;
        }

        public string FullPath(string path, string name)
        {
            var root = Root;
            root += (string.IsNullOrEmpty(path)) ? "" : "\\" + path;
            root += (string.IsNullOrEmpty(name)) ? "" : "\\" + name;
            return root;
        }

        #region Folder
        public bool DirectoryExists(string path, string name)
        {
            return System.IO.Directory.Exists(FullPath(path, name));
        }

        private Folder AllFolder(Folder folder)
        {
            long idCount = 0;
            Folder rootFolder = new Folder() { Id = idCount, Name = folder.Name, PathFromRoot = folder.PathFromRoot };

            var stack = new Stack<Folder>();
            stack.Push(rootFolder);

            while (stack.Count > 0)
            {
                Folder currentFolder = stack.Pop();

                string pathFromRoot = "";
                pathFromRoot += (String.IsNullOrEmpty(currentFolder.PathFromRoot))
                    ? ""
                    : "\\" + currentFolder.PathFromRoot;
                pathFromRoot += (String.IsNullOrEmpty(currentFolder.Name))
                    ? ""
                    : "\\" + currentFolder.Name;

                foreach (var directory in Directory.GetDirectories(Root + "\\" + pathFromRoot))
                {
                    var childFolder = new Folder()
                    {
                        Id = ++idCount,
                        Name = directory.Split('\\').ToList().Last(),
                        PathFromRoot = pathFromRoot.Trim(new char[] { '\\' }),
                    };
                    currentFolder.Children = currentFolder.Children ?? new List<Folder>();
                    currentFolder.Children.Add(childFolder);
                    stack.Push(childFolder);
                }
            }

            return rootFolder;
        }

        public ApiResult AllFolderFromRoot()
        {
            DirectoryInfo directory = new DirectoryInfo(Root);
            if (!directory.GetFiles().Any() && !directory.GetDirectories().Any())
            {
                FileStream stream = null;
                try
                {
                    stream = File.Create(String.Format("{0}\\{1}", Root, _defaultFile));
                }
                catch
                {
                    throw new Exception("unable to create default file at root");
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close(); 
                    }
                }
            }

            Folder folder = AllFolder(new Folder() {Name = "", PathFromRoot = ""});
            return new ApiResult(folder);
        }

        public ApiResult AddDerectory(string path, string name)
        {
            if (DirectoryExists(path, name))
            {
                return new ApiResult((int)ErrorEnum.UnprocessableEntity, "folder exists.");
            }
            Directory.CreateDirectory(FullPath(path, name));

            return new ApiResult(0, "Ok");
        }
        public ApiResult RenameDerectory(string path, string oldName, string name)
        {
            if (!DirectoryExists(path, oldName))
            {
                throw new Exception("folder doesn't exist.");
            }
            if ((oldName != name) && DirectoryExists(path, name))
            {
                return new ApiResult((int)ErrorEnum.UnprocessableEntity, "folder exists.");
            }

            var oldFullPath = FullPath(path, oldName);
            var newFullPath = FullPath(path, name);
            if (oldFullPath.ToLower() == newFullPath.ToLower())
            {
                return new ApiResult(0, "Ok");
            }

           Directory.Move(FullPath(path, oldName), FullPath(path, name));

           return new ApiResult(0, "Ok");
        }
        public ApiResult RemoveDerectory(string path, string name)
        {
            if (!DirectoryExists(path, name))
            {
                throw new Exception("folder doesn't exist.");
            }
            System.IO.Directory.Delete(FullPath(path, name));

            return new ApiResult(0, "Ok");
        }
        #endregion

        #region File
        public bool FileExists(string path, string name)
        {
            return System.IO.File.Exists(FullPath(path, name));
        }

        //public List<string> AllFolder(string path)
        //{
        //    return Directory.GetDirectories(FullPath(path, ""), "*.*", SearchOption.AllDirectories).ToList();
        //}
        public ApiResult AllFile(string path, string folderName)
        {
            var pathFromRoot = "";
            pathFromRoot += (string.IsNullOrEmpty(path)) ? "" : "\\" + path;
            pathFromRoot += (string.IsNullOrEmpty(folderName)) ? "" : "\\" + folderName;

            var directory = new DirectoryInfo(FullPath(path, folderName));
            List<Content> contents = (from obj in directory.GetFiles()
                                      select new Content()
                                      {
                                          Name = obj.Name,
                                          PathFromRoot = pathFromRoot.Trim(new char[] { '\\' }),
                                          Type = obj.Extension,
                                          ModifiedDateTime = obj.LastWriteTime,
                                          Size = obj.Length
                                      }).ToList();
            return new ApiResult(contents);
        }
        public ApiResult RemoveFile(string path, string name)
        {
            if (!FileExists(path, name))
            {
                throw new Exception("file not found.");
            }
            System.IO.File.Delete(FullPath(path, name));

            return new ApiResult(0, "Ok");
        }
        public ApiResult RenameFile(string path, string oldName, string name)
        {
            if (!FileExists(path, oldName))
            {
                throw new Exception("Old file doesn't exist.");
            }
            if ((oldName != name) && FileExists(path, name))
            {
                return new ApiResult((int)ErrorEnum.UnprocessableEntity, "File exists.");
            }
            File.Move(FullPath(path, oldName), FullPath(path, name));

            return new ApiResult(0, "Ok");
        }
        #endregion
    }
}
